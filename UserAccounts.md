#### Create user accounts
1. use firebase auth with google oauth
2. save the persisted storeage to google
   - how to send incremental data periodically?, lets say every minute
3. fetch this data at the time of login.
   - how to merge offline work ?

##### merge stategy
1. Keep the last created and synced to firebase note id.
2. In case of diffs, ask whether to keep cloud version or your version if note id less than last synced id.
3. otherwise keep both notes and modify the id of local notes.
