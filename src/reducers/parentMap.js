/* state json will be of format
{
  "1": "0",
  "2": "0"
}
*/

const parentMap = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      let changes = {}
      changes[action.id] = action.parentId
      return {
        ...state,
        ...changes
      };
    case 'MOVE_TODO':
      changes = {}
      changes[action.moveInfo.todoId] = action.moveInfo.newRoot
      return {
        ...state,
        ...changes
      }
    case 'DELETE_TODO':
      let newState = {
        ...state
      }
      delete newState[action.id]
      return newState
    default:
      return state;
  }
}

export default parentMap
