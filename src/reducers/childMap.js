/* state json will be of format
{
  "0": [],
  "1": []
}
*/

const remove = (arr, v) => {
  if (!arr || arr.constructor !== Array)
    return arr;
  if (arr.indexOf(v) < 0) {
    return arr;
  }
  let newArr = [];
  arr.map(k => {
    if (k !== v)
      newArr.push(k);
    }
  )
  return newArr;
}

const add = (arr, v, index) => {
  if (!arr || arr.constructor !== Array)
    return arr;
  if (arr.indexOf(v) >= 0) {
    return arr;
  }
  let newArr = [];
  arr.map(k => {
    newArr.push(k);
  })
  newArr.splice(index, 0, v)
  return newArr;
}

const childMap = (state = {
  0: []
}, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      if (state[action.parentId]) {
        var updatedNode = {};
        updatedNode[action.parentId] = [
          ...state[action.parentId],
          action.id
        ];
        updatedNode[action.id] = [];
        return {
          ...state,
          ...updatedNode
        };
      }
      return state;
    case 'DELETE_TODO':
      var newState = {};
      Object.keys(state).forEach(key => {
        newState[key] = remove(state[key], action.id);
      });
      return newState;
    case 'MOVE_TODO':
      if (action.moveInfo.currentRoot === action.moveInfo.newRoot) {
        if (action.moveInfo.position === action.moveInfo.currentPosition) {
          return state;
        }
        if (action.moveInfo.currentPosition < action.moveInfo.position) {
          action.moveInfo.position = action.moveInfo.position - 1;
        }
      }
      if (state[action.moveInfo.currentRoot] && state[action.moveInfo.newRoot]) {
        var updatedNode = {}
        updatedNode[action.moveInfo.currentRoot] = remove(state[action.moveInfo.currentRoot], action.moveInfo.todoId)
        var newState = {
          ...state,
          ...updatedNode
        }
        updatedNode[action.moveInfo.newRoot] = add(
          newState[action.moveInfo.newRoot],
          action.moveInfo.todoId,
          action.moveInfo.position
        )
        return {
          ...newState,
          ...updatedNode
        }
      }
      return state;
    default:
      return state;
  }
}

export default childMap
