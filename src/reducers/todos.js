/* state json will be of format
{
  "0": {"id": "0", "title": "", "text": "", "newTitle": "", "newText": ""},
  "1": {}
}
*/
const todo = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return {id: action.id, title: action.title, completed: false, edit: false, hide: true, renderHtml: false}
    case 'UPDATE_TODO':
      return {
        ...state,
        id: action.id,
        title: state.newTitle,
        text: state.newText,
        completed: false,
        edit: false
      }
    case 'CANCEL_EDIT':
      return {
        ...state,
        id: action.id,
        newTitle: state.title,
        newText: state.text,
        completed: false,
        edit: false
      }
    case 'EDIT_TODO':
      return {
        ...state,
        id: action.id,
        newTitle: state.title,
        newText: state.text,
        completed: false,
        edit: true
      }
    case 'HIDE_TODO':
      return {
        ...state,
        hide: action.hide
      }
    case 'UPDATE_TITLE':
      return {
        ...state,
        id: action.id,
        newTitle: action.title
      }
    case 'UPDATE_TEXT':
      return {
        ...state,
        id: action.id,
        newText: action.text
      }
    case 'UPDATE_RENDER_HTML':
      return {
        ...state,
        id: action.id,
        renderHtml: action.renderHtml
      }
    case 'TOGGLE_TODO':
      if (state.id !== action.id) {
        return state
      }

      return Object.assign({}, state, {
        completed: !state.completed
      })

    default:
      return state
  }
}

const todos = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      var todos = {};
      todos[action.id] = todo(undefined, action);
      return {
        ...state,
        ...todos
      }
    case 'UPDATE_TODO':
    case 'UPDATE_TITLE':
    case 'UPDATE_TEXT':
    case 'CANCEL_EDIT':
    case 'EDIT_TODO':
    case 'UPDATE_RENDER_HTML':
      todos = {};
      todos[action.id] = todo(state[action.id], action);
      return {
        ...state,
        ...todos
      }
    case 'DELETE_TODO':
      todos = {
        ...state
      };
      delete todos[action.id];
      return todos;
    case 'TOGGLE_TODO':
      return state.map(t => todo(t, action))
    case 'HIDE_TODO':
      todos = {};
      for(var id in action.ids){
        todos[action.ids[id]] = todo(state[action.ids[id]], action)
      }
      return {
        ...state,
        ...todos
      }
    default:
      return state
  }
}

export default todos
