/*state json will be of format
{
  "selectedTodoId": 1,
  "nextTodoId": 2,
  "splitPaneSize": 400
}
*/
const todotree = (state = {splitPaneSize: 400}, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      let nextTodoId = state.nextTodoId
        ? (parseInt(state.nextTodoId) + 1)
        : 2;
      return {
        ...state,
        nextTodoId: nextTodoId
      }
    case 'SELECT_TODO':
      return {
        ...state,
        selectedTodoId: action.id
      }
    case 'SPLIT_PANE_SIZE_CHANGE':
      return {
        ...state,
        splitPaneSize: action.size
      }
    case 'DELETE_TODO':
    default:
      return state;
  }
}

export default todotree
