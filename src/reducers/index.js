import { combineReducers } from 'redux'
import childMap from './childMap'
import parentMap from './parentMap'
import todos from './todos'
import support from './support'

const todoApp = combineReducers({ childMap, parentMap, todos, support })

export default todoApp
