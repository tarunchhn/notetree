import {connect} from 'react-redux'
import {toggleTodo, selectTodo, hideTodo} from '../actions'
import Todo from '../components/todo'

const mapStateToProps = (state, ownProps) => {
  return {
    todo: state.todos[ownProps.id],
    parentId: ownProps.parentId,
    selectedTodoId: ownProps.selectedTodoId
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onTodoClick: (id) => dispatch(toggleTodo(id)),
    onSelectTodo: (id) => dispatch(selectTodo(id)),
    hide: (ids, hide) => dispatch(hideTodo(ids, hide))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Todo);
