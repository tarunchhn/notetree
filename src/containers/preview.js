import { connect } from 'react-redux'
import { editTodo } from '../actions'
import Preview from '../components/preview'

const mapStateToProps = (state, ownProps) => {
  return {};
}

const mapDispatchToProps = (dispatch) => {
  return {
    editTodo: (id) => dispatch(editTodo(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Preview)
