import React from 'react'
import { connect } from 'react-redux'
import { addTodo } from '../actions'

let AddTodo = ({ dispatch, parentId, nextTodoId, hide }) => {
  let input, form;

  return (
    <div className={hide ? "d-none" : ""}>
      <form
        className="form ml-2"
        ref={node => {
          form = node
        }}>

        <input
            type="text" data-toggle="tooltip" data-placement="bottom" title="Type and press enter to create a new note"
            aria-describedby="basic-addon1"
            className="form-control"
            placeholder="new note"
            onKeyDown={e => {if (e.keyCode == 13) {
              e.preventDefault();
              if (!input.value.trim()) {
                return;
              }
              dispatch(addTodo(nextTodoId ? nextTodoId : 1, input.value, parentId));
              input.value = '';
              return false; }}}
            ref={node => {
            input = node
          }}/>
      </form>
    </div>
  )
}
const mapStateToProps = (state, ownProps) => {
  return {
    todo: state.todos[ownProps.id],
    parentId: ownProps.parentId,
    selectedTodoId: ownProps.selectedTodoId
  }
}
AddTodo = connect(mapStateToProps)(AddTodo)

export default AddTodo
