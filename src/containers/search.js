import {connect} from 'react-redux'
import {selectTodo} from '../actions'
import Search from '../components/search'

const mapStateToProps = (state, ownProps) => {
  var todos = [];
  for (var key in state.todos) {
    todos.push(state.todos[key]);
  }
  return {todos: todos, selectedTodoId: state.support.selectedTodoId};
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSelectTodo: (id) => {
      dispatch(selectTodo(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)
