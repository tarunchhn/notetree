import {connect} from 'react-redux';
import {toggleTodo} from '../actions'
import Tree from '../components/tree'

const mapStateToProps = (state, ownProps) => {
  return {
    tree: state.childMap[ownProps.root],
    root: ownProps.root,
    hide: ownProps.hide,
    selectedTodoId: state.support.selectedTodoId,
    nextTodoId: state.support.nextTodoId
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onTodoClick: (id) => {
      dispatch(toggleTodo(id))
    }
  }
}

const VisibleTodoList = connect(mapStateToProps, mapDispatchToProps)(Tree)

export default VisibleTodoList
