import { connect } from 'react-redux'
import { updateTitle, updateText } from '../actions'
import Details from '../components/details'

const mapStateToProps = (state, ownProps) => {
  return {
    todo: state.todos[state.support.selectedTodoId],
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateTodo: (todo) => dispatch(updateTodo(todo))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Details)
