import { connect } from 'react-redux'
import { updateTodo, deleteTodo, editTodo, cancelEdit, hideTodo, updateRenderHtml } from '../actions'
import Header from '../components/header'

const mapStateToProps = (state, ownProps) => {
    var parents = [];
    var selectedTodoId = state.support.selectedTodoId;
    var i = parseInt(state.parentMap["" + selectedTodoId]);
    while (i > 0) {
      parents.push(parseInt(i));
      i = parseInt(state.parentMap["" + i]);
    }
  return {
    todo: state.todos[state.support.selectedTodoId],
    parents: parents
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateTodo: (id) => dispatch(updateTodo(id)),
    deleteTodo: (id) => dispatch(deleteTodo(id)),
    cancelEdit: (id) => dispatch(cancelEdit(id)),
    editTodo: (id) => dispatch(editTodo(id)),
    renderHtml: (id, renderHtml) => dispatch(updateRenderHtml(id, renderHtml)),
    hide: (ids, hide) => dispatch(hideTodo(ids, hide))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)
