import { connect } from 'react-redux'
import { updateTitle, updateText } from '../actions'
import Editor from '../components/editor'

const mapStateToProps = (state, ownProps) => {
  return {
    todo: state.todos[state.support.selectedTodoId]
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateTitle: (id, title) => dispatch(updateTitle(id, title)),
    updateText: (id, text) => dispatch(updateText(id, text))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Editor)
