import {connect} from 'react-redux'
import {updateTitle, updateText, selectTodo} from '../actions'
import Details from '../components/details'

const mapStateToProps = (state, ownProps) => {
  var parents = [];
  var selectedTodoId = state.support.selectedTodoId;
  var i = parseInt(state.parentMap["" + selectedTodoId]);
  while (i > 0) {
    parents.push({
      title: state.todos["" + i].title,
      id: parseInt(i)
    });
    i = parseInt(state.parentMap["" + i]);
  }
  return {
    todo: state.todos[state.support.selectedTodoId],
    parents: parents
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateTitle: (id, title) => dispatch(updateTitle(id, title)),
    updateText: (id, text) => dispatch(updateText(id, text)),
    onSelectTodo: (id) => dispatch(selectTodo(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Details)
