import { connect } from 'react-redux'
import { moveTodo } from '../actions'
import TodoReceiver from '../components/todoReceiver'

const mapStateToProps = (state, ownProps) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    moveTodo: (moveInfo) => dispatch(moveTodo(moveInfo))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoReceiver)
