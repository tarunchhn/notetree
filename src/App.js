import React, { Component } from 'react';
//import './App.css';
import { Provider } from 'react-redux';
import { compose, createStore } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist'
import configureStore from './configureStore'
import { PersistGate } from 'redux-persist/integration/react'
import Notes from './notes'


let {store, persistor} = configureStore()
// main app class, the top level container
class App extends Component {

  constructor() {
    super()
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Notes />
      </PersistGate>
      </Provider>
    );
  }
}

export default App;
