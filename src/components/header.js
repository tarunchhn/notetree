import React, {PropTypes, Component} from 'react';
import {MessageModal} from './util'
import Help from './help'
import $ from 'jquery';

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      showHelp: false
    }

    this.closeModal = this.closeModal.bind(this);
    this.changeTheme = this.changeTheme.bind(this);
  }

  changeTheme(theme){
    var originaltheme = localStorage.getItem('theme');
    if (!originaltheme) {
      originaltheme = "theme-green";
    }
    if(originaltheme !== theme){
      localStorage.setItem('theme', theme);
      $('link#themeFile').remove();
      $('head').append('<link id="themeFile" rel="stylesheet" type="text/css" href="static/css/' + theme + '.css">');
    }
  }

  closeModal() {
    this.setState({showModal: false});
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-dark nav-theme fixed-top py-0 navbar-expand-lg">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="navbar-brand dropdown">
            <button className="btn btn-outline-light dropdown-toggle" id="dropdownMenuBrand" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={{
                cursor: "default"
              }}><i className="fa fa-plus-square"/>
              &nbsp;Note</button>

            <div class="dropdown-menu dropright" aria-labelledby="dropdownMenuBrand">
              <button class="btn dropdown-item dropdown-toggle" id="dropdownThemes" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Themes</button>
              <div className="dropdown-menu" aria-labelledby="dropdownThemes">
                <button className="dropdown-item" onClick={e => this.changeTheme("theme-green")}>Green</button>
                <button className="dropdown-item" onClick={e => this.changeTheme("theme-dark")}>Dark</button>
              </div>
            </div>
          </div>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item ml-2">
                <button className="btn btn-outline-light" href="#">Import</button>
              </li>
              <li className="nav-item ml-2">
                <button className="btn btn-outline-light" href="#">Export</button>
              </li>
            </ul>
          </div>
          <div className="btn-group float-right">
            {
              this.props.todo && <div className="btn-group">
                  <i
                    className={"fas fa-h-square".concat(
                      this.props.todo.renderHtml
                        ? " active"
                        : ""
                    )}
                    onClick={e => this.props.renderHtml(this.props.todo.id, !this.props.todo.renderHtml)}
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Render html in note"/>
                  <i
                    className="far fa-dot-circle ml-2"
                    onClick={e => this.props.hide(this.props.parents, false)}
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Locate Note in tree"/>
                  <i
                    className={"far fa-edit ml-2".concat(
                      this.props.todo.edit
                        ? " d-none"
                        : ""
                    )}
                    onClick={e => this.props.editTodo(this.props.todo.id)}
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Edit"/>
                  <i
                    className={"far fa-save ml-2".concat(
                      this.props.todo.edit
                        ? ""
                        : " d-none"
                    )}
                    onClick={e => this.props.updateTodo(this.props.todo.id)}
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Save"/>
                  <i
                    className={"fas fa-times ml-2".concat(
                      this.props.todo.edit
                        ? ""
                        : " d-none"
                    )}
                    onClick={e => this.props.cancelEdit(this.props.todo.id)}
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Discard changes"/>
                  <i
                    className="fas fa-trash ml-2"
                    onClick={e => {
                      this.setState({showModal: true})
                    }}
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Delete"/>
                </div>
            }
            <i
              className="far fa-question-circle ml-2"
              data-toggle="tooltip"
              data-placement="bottom"
              title="Help"
              onClick={e => {
                this.setState({
                  showHelp: !this.state.showHelp
                })
              }}/>
          </div>
        </nav>
        <Help showHelp={this.state.showHelp}/>
        <MessageModal
          isOpen={this.state.showModal}
          message="Are you sure you want to delete this todo?"
          close={e => {
            this.closeModal()
          }}
          yes={e => {
            this.props.deleteTodo(this.props.todo.id);
            this.closeModal()
          }}/>
      </div>
    );
  }

}
