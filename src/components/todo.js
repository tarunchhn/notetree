import React, {PropTypes, Component} from 'react';
import Tree from '../containers/tree.js';
import './todo.css'
import {DragSource} from 'react-dnd';
import {ItemTypes} from './util'
import {CSSTransition} from 'react-transition-group'

const todoSource = {
  beginDrag(props) {
    return {todoId: props.todo.id, currentRoot: props.parentId, currentPosition: props.position};
  }
};

function collect(connect, monitor) {
  return {connectDragSource: connect.dragSource(), isDragging: monitor.isDragging()}
}

class Todo extends Component {

  render() {
    const {todo, onClick, connectDragSource, isDragging} = this.props;
    let hide = todo.hide === undefined
      ? true
      : todo.hide;
    return connectDragSource(
      <li onClick={onClick} style={{
          fontSize: "large"
        }}>
        <CSSTransition in={true} appear={true} mountOnEnter={true} timeout={1000} classNames="star">
          <div className="d-table">
            <i
              className={"d-table-cell p-2 fas fa-chevron-".concat(
                hide
                  ? "right"
                  : "down"
              ).concat(
                this.props.selectedTodoId === todo.id
                  ? " selected"
                  : ""
              )}
              style={{
                fontSize: "small"
              }}
              onClick={e => {
                e.preventDefault();
                this.props.hide([todo.id], !hide);
              }}/>
            <span
              onClick={e => {
                e.preventDefault();
                this.props.onSelectTodo(todo.id);
              }}
              className={"d-table-cell text-truncate".concat(
                this.props.selectedTodoId === todo.id
                  ? " selected"
                  : ""
              )}
              style={{
                cursor: "pointer"
              }}>{todo.title}</span>
          </div>
        </CSSTransition>
        <Tree root={todo.id} hide={hide}/>
      </li>
    );
  }
}

export default DragSource(ItemTypes.TODO, todoSource, collect)(Todo);
