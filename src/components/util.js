import React, { Component } from 'react';
import Modal from 'react-modal';

const modalStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    border: 'none',
    background: 'transparent'
  }
};

class MessageModal extends Component {

  constructor(props) {
    super(props);

    this.onAfterOpen = this.onAfterOpen.bind(this);
  }

  onAfterOpen() {}

  render() {
    return (
      <Modal isOpen={this.props.isOpen}
        style={modalStyles}
        contentLabel="Message Modal"
        onAfterOpen={this.onAfterOpen}>
        <div className="modal-content">
          <div className="modal-header">
            <h4 className="modal-title">Message</h4>
            <button type="button" className="close" onClick={this.props.close}
              data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div className="modal-body">
            <p>{this.props.message}</p>
          </div>
          <div className="modal-footer">
            <button className="btn btn-primary float-right" onClick={this.props.yes}>Yes</button>
            <button className="btn btn-default float-right" onClick={this.props.close}>Cancel</button>
          </div>
        </div>
      </Modal>
    );
  }
}

const ItemTypes = {
  TODO: 'todo'
};

const copyData = function(e, data) {
  var textField = document.createElement('textarea');
  console.log(data);
  var textNode = document.createTextNode(data);
  textField.append(textNode);
  document.body.appendChild(textField);
  textField.select();
  try {
    document.execCommand('copy');
  } catch (err) {}
  textField.remove();
};

export { MessageModal, ItemTypes, copyData };
