import React, {Component} from 'react';
import Todo from '../containers/todo'
import TodoReceiver from '../containers/todoReceiver'
import AddTodo from '../containers/addTodo';
import './tree.css';

export default class Tree extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ul
        className={"border-left".concat(
          this.props.hide
            ? " d-none"
            : ""
        ).concat(
          this.props.root === this.props.selectedTodoId
            ? " bg-noteSelected"
            : ""
        )}>
        {
          this.props.tree.map(
            (v, k) => <div key={k}>
              <TodoReceiver position={k} root={this.props.root}/>
              <Todo position={k} id={v} selectedTodoId={this.props.selectedTodoId} parentId={this.props.root} onClick={() => {}}/>
            </div>
          )
        }
        <TodoReceiver position={this.props.tree.length} root={this.props.root}/>
        <AddTodo parentId={this.props.root} nextTodoId={this.props.nextTodoId} hide={this.props.root != 0 && this.props.root != this.props.selectedTodoId}/>
      </ul>
    );
  }

}
