import React, { PropTypes, Component } from 'react';
import Editor from '../components/editor'
import Preview from '../components/preview'
import { DropTarget } from 'react-dnd';
import { ItemTypes } from './util'

const todoTarget = {
  drop(props, monitor) {
    props.moveTodo({
      currentRoot: monitor.getItem().currentRoot,
      currentPosition: monitor.getItem().currentPosition,
      todoId: monitor.getItem().todoId,
      newRoot: props.root,
      position: props.position
    });
  }
}

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

class TodoReceiver extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { connectDropTarget, isOver } = this.props;
    return connectDropTarget(
      <div className={isOver ? "py-2 bg-primary" : "py-1"}>
      </div>
    );
  }

}

export default DropTarget(ItemTypes.TODO, todoTarget, collect)(TodoReceiver);
