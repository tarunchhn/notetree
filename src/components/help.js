import React, {Component} from 'react';
import './help.css'
var ReactMarkdown = require('react-markdown');

const helpText = `
## Toolbar
1. <i class="fas fa-plus-square text-help"/> Note - adds a new note as first element at topmost (root) level
2. Import - import a tree of notes at topmost (root) level
3. Export - exports a selected note and its children in json format
4. <i class="fas fa-h-square text-help"/> - renders the html written in notes, not perfect, only some of html features may work
4. <i class="far fa-dot-circle text-help"/> - locates the note in the tree on the left pane
5. <i class="far fa-edit text-help"/> - edits a note
6. <i class="far fa-save text-help"/> - saves the note
7. <i class="fas fa-times text-help"/> - discards the changes done in the note
8. <i class="fas fa-trash text-help"/> - deletes a note
9. <i class="far fa-question-circle text-help"/> - help

## Left Pane
* Search box - type anything and it will start showing results, its a fuzzy Search
* Notes
  - clicking on a note title will display the note in right pane.
  - clicking on the > icon will show the children on that note.
  - type anything in new note box and pressing enter will create a new note, this box is available in all the levels, however you can add a note only at top level or below the selected note
  - you can drag and drop a note from one position to another in same level or in another level.

## Right Pane
If note being shown is being edited (using the icon in header), a editor will be shown, otherwise the note (formatted) will be shown.
The editor accepts markdown syntax.

## Markdown Syntax
Markdown cheatsheet can be found [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

copying some of the examples from the above cheatsheet for quick reference

### Typography
#### Headers
##### Editor content
\`\`\`markdown
# H1
## H2
### H3
#### H4
##### H5
###### H6
\`\`\`
##### Note content
# H1
## H2
### H3
#### H4
##### H5
###### H6

#### Emphasis
##### Editor content
\`\`\`markdown
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~
\`\`\`
##### Note content
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

`
export default class Help extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showHelpText: false
    }
  }

  render() {
    return (
      <div
        className={"container position-absolute bg-primary text-help help".concat(
          this.props.showHelp
            ? " slide-in"
            : " slide-out"
        )}
        style={{
          zIndex: "1000",
          top: "50px",
          bottom: "0",
          right: "0",
          overflow: "scroll",
        }}>
        <div className="row">
          <div className="col">
            <div>
              <button
                className="btn btn-link"
                onClick={e => {
                  this.setState({showHelpText: true})
                }}>show source</button>
              <ReactMarkdown source={helpText} escapeHtml={false}/>
            </div>
          </div>
          <div className={"col".concat(this.state.showHelpText ? "" : " d-none")}>
            <button className="btn btn-link" onClick={e => {
                this.setState({showHelpText: false})
              }}>hide source</button>
            <pre style={{whiteSpace: "pre-wrap"}}>
              <code className="text-help">
                {helpText}
              </code>
            </pre>
          </div>
        </div>
      </div>
    );
  }

}
