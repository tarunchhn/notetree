import React, {PropTypes, Component} from 'react';

let table = `\n
| head |  head   |head|
|------|---------|----|
|cell  | cell    |cell|
|cell  | cell    |cell|
|cell  | cell    |cell|
\n`
export default class EditorToolBox extends Component {

  render() {
    return (
      <div className="mb-3">
        <div className="m-1 d-inline">
          <i
            className="p-1 tool"
            onClick={e => this.props.insertText("\n# Header1\n")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Header 1">H1</i>
          <i
            className="p-1 tool"
            onClick={e => this.props.insertText("\n## Header2\n")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Header 2">H2</i>
          <i
            className="p-1 tool"
            onClick={e => this.props.insertText("\n### Header3\n")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Header 3">H3</i>
          <i
            className="p-1 tool"
            onClick={e => this.props.insertText("\n#### Header4\n")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Header 4">H4</i>
          <i
            className="p-1 tool"
            onClick={e => this.props.insertText("\n##### Header5\n")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Header 5">H5</i>
          <i
            className="p-1 tool"
            onClick={e => this.props.insertText("\n###### Header6\n")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Header 6">H6</i>
        </div>
        <div className="m-1 d-inline">
          <span
            className="p-1 tool"
            onClick={e => this.props.insertText("**bold**")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Bold">
            <strong>B</strong>
          </span>
          <i
            className="p-1 tool"
            onClick={e => this.props.insertText("*Italics*")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Italic"> I </i>
          <span
            className="p-1 tool"
            onClick={e => this.props.insertText("~~StrikeThrough~~")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Strike through">
            <s>St</s>
          </span>
        </div>
        <div className="m-1 d-inline">
          <i
            className="p-1 fas fa-list-ul text-muted tool"
            onClick={e => this.props.insertText("\n* listitem")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="List"></i>
          <i
            className="p-1 fas fa-list-ol text-muted tool"
            onClick={e => this.props.insertText("\n1. listitem")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Numbered List"></i>
        </div>
        <div className="m-1 d-inline">
          <i
            className="p-1 fas fa-th text-muted tool"
            onClick={e => this.props.insertText(table)}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Table"></i>
        </div>
        <div className="m-1 d-inline">
          <i
            className="p-1 fas fa-link text-muted tool"
            onClick={e => this.props.insertText("[here](https://example.com)")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Link"></i>
        </div>
        <div className="m-1 d-inline">
          <i
            className="p-1 fas fa-angle-double-right text-muted tool"
            onClick={e => this.props.insertText("\n> quoted text")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Backquote"></i>
        </div>
        <div className="m-1 d-inline">
          <i
            className="p-1 fas fa-code text-muted tool"
            onClick={e => this.props.insertText("\n```javascript\nlet a = b;\n```")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Code"></i>
        </div>
        <div className="m-1 d-inline">
          <i
            className="p-1 fas fa-quote-left text-muted tool"
            onClick={e => this.props.insertText("`let a = b;`")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Inline Code"></i>
        </div>
        <div className="m-1 d-inline">
          <i
            className="p-1 fas fa-image text-muted tool"
            onClick={e => this.props.insertText("![alt text](https://example.com/image.jpg \"hover or alternate text\")")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="Image"></i>
        </div>
        <div className="m-1 d-inline">
          <i
            className="p-1 text-muted tool"
            onClick={e => this.props.insertText("\n---\n")}
            data-toggle="tooltip"
            data-placement="bottom"
            title="horizontal rule">-</i>
        </div>
      </div>
    );
  }

}
