import React, {Component} from 'react';
import {EditorState, convertToRaw, ContentState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

export default class RichEditor extends Component {
  state = {
    editorState: this.props.editorState
  }

  onEditorStateChange: Function = (editorState) => {
    this.setState({editorState});
  };

  render() {
    if (this.props.save) {
      this.props.updateTodo({id: this.props.todoId, title: this.state.title, editorState: this.state.editorState});
      return null;
    }
    const {editorState} = this.state;
    return (
      <div>

        <div className="btn-group float-right">
          <i
            className={"fa fa-edit ml-2".concat(
              this.props.todo.edit
                ? " d-none"
                : ""
            )}
            onClick={e => this.props.editTodo(this.props.todo.id)}/>
          <i
            className={"fa fa-save ml-2".concat(
              this.props.todo.edit
                ? ""
                : " d-none"
            )}
            onClick={e => this.props.updateTodo(this.props.todo.id)}/>
          <i
            className={"fa fa-times ml-2".concat(
              this.props.todo.edit
                ? ""
                : " d-none"
            )}
            onClick={e => this.props.cancelEdit(this.props.todo.id)}/>
          <i className="fa fa-trash ml-2" onClick={e => {
              this.setState({showModal: true})
            }}/>
        </div>
        <Editor
          editorState={editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={this.onEditorStateChange}/>
        {/* <textarea disabled="disabled" value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}/> */}
      </div>
    );
  }
}
