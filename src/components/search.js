import React, {Component} from 'react';
import Fuse from 'fuse.js';

var items = [];

const fuseOptions = {
  shouldSort: true,
  includeScore: true,
  threshold: 0.4,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 2,
  keys: [
    {
      name: 'title',
      weight: 0.7
    }, {
      name: 'text',
      weight: 0.3
    }
  ]
};

var fuse = new Fuse(items, fuseOptions);

export default class Tree extends Component {

  constructor(props) {
    super(props);
    this.state = {
      todos: []
    }
    this.search = this.search.bind(this);
    fuse = new Fuse(this.props.todos, fuseOptions);
  }

  search(event) {
    var todoIds = [];
    var searchResults = fuse.search(event.target.value);
    this.setState({todos: searchResults});
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    fuse = new Fuse(this.props.todos, fuseOptions);
  }

  render() {
    return (
      <div>
          <div className="form-row align-items-center m-2">
            <div className="input-group">
              <div className="input-group-prepend">
                <div className="input-group-text bg-primary-50"><i className="fas fa-search text-primary"/></div>
              </div>
              <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="search" onChange={this.search}/></div>
          </div>
        {this.state.todos.length > 0 && <h4 className="text-primary mt-2 ml-1">Search Results</h4>}
        <ul className="list-group list-group-flush">
          {
            this.state.todos.map(
              (v, k) => <li style={{border: 0}}
                className={"list-group-item list-group-item-action text-truncate".concat(
                  this.props.selectedTodoId === v.item.id
                    ? " selected"
                    : ""
                )}
                key={k}
                onClick={e => {
                  e.preventDefault();
                  this.props.onSelectTodo(v.item.id);
                }}>{v.item.title} - {v.item.text}</li>
            )
          }
        </ul>
        {this.state.todos.length > 0 && <h4 className="text-primary mt-2 ml-1">All Notes</h4>}
      </div>
    );
  }

}
