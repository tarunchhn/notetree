import React, { PropTypes, Component } from 'react';
import './editor.css';
import { MessageModal } from './util'
import EditorToolBox from './editorToolBox'
var ReactDOM = require('react-dom');
var ReactMarkdown = require('react-markdown');

let titleInput, textInput, textAreaNode;
export default class Editor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.todo.newTitle,
      text: this.props.todo.newText
    }

    this.startEditor = this.startEditor.bind(this);
    this.insertText = this.insertText.bind(this);
  }

  startEditor() {
    this.setState({ title: this.props.todo.title, text: this.props.todo.text });
  }

  insertText(text){
    let startPos = textAreaNode.selectionStart;
    let endPos = textAreaNode.selectionEnd;
    textAreaNode.value = textAreaNode.value.substring(0, startPos) + text + textAreaNode.value.substring(endPos, textAreaNode.value.length);
    textAreaNode.selectionStart = startPos + text.length;
    textAreaNode.selectionEnd = startPos + text.length;
    this.setState({text: textAreaNode.value});
    this.props.updateText(this.props.todo.id, textAreaNode.value);
    textAreaNode.focus();
  }

  render() {
    let form;
    const { todo } = this.props;
    if (!todo) return null;
    return (
      <div className={"full-height ".concat(this.props.className)}>
        <form
          ref={node => {form = node}}
          className="form-horizontal full-height">

            <div className="form-group">
              <label>Title</label>
              <input id="title"
                type="text"
                className="form-control mb-1 ml-0"
                style={{fontSize: "xx-large"}}
                value={this.state.title}
                onChange={e => {this.setState({title: e.target.value}); this.props.updateTitle(this.props.todo.id, e.target.value)}}
              />

            </div>
            <div className="pt-2 full-height">
              <div className="form-group full-height">
                <label>Markdown editor</label>
                <EditorToolBox insertText={this.insertText}/>
                <textarea className="p-2 border rounded" ref={node => textAreaNode = node}
                  style={{width: '100%', height: '100%'}}
                  value={this.state.text}
                  onChange={e => { this.setState({text: e.target.value}); this.props.updateText(this.props.todo.id, e.target.value);}}/>
              </div>
            </div>
        </form>
      </div>
    );
  }

  static getDerivedStateFromProps(props, state) {
    return {
      title: props.todo.newTitle === undefined ? '' : props.todo.newTitle,
      text: props.todo.newText === undefined ? '' : props.todo.newText
    }
  }

}
