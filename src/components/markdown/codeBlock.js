const hljs = require('highlight.js')
const React = require('react')
const PropTypes = require('prop-types')

class CodeBlock extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      collapse: false
    }
    this.setRef = this.setRef.bind(this)
    this.copyCode = this.copyCode.bind(this)
  }

  setRef(el) {
    this.codeEl = el
  }

  componentDidMount() {
    this.highlightCode()
  }

  componentDidUpdate() {
    this.highlightCode()
  }

  highlightCode() {
    this.codeEl && hljs.highlightBlock(this.codeEl)
  }

  copyCode(e) {
    var textField = document.createElement('textarea');
    console.log(this.props.value);
    var textNode = document.createTextNode(this.props.value);
    textField.append(textNode);
    document.body.appendChild(textField);
    textField.select();
    try {
      document.execCommand('copy');
    } catch (err) {}
    textField.remove();
  };

  render() {
    return (
      <div className="border border-grey rounded mb-1 w-100">
        <div>
          <div
            className="btn border-0 m-1 text-primary"
            onClick={e => this.setState({
              collapse: !this.state.collapse
            })}>{
              this.state.collapse
                ? <span><i className="fas fa-chevron-right text-primary"/>
                    &nbsp;expand</span>
                : <span><i className="fas fa-chevron-down text-primary"/>
                    &nbsp;collapse</span>
            }</div>
          <div className="float-right">
            <button className="btn btn-outline-primary border-0 m-1 float-right" onClick={this.copyCode}>copy</button>
          </div>
        </div>
        {/* wrap long text and break long words into multiple lines */}
        {
          this.state.collapse || <pre className="border-0" style={{whiteSpace: "pre-wrap"}}>
          <code ref={this.setRef} className={`language-${this.props.language}`}
            style={{ wordBreak: "break-all"}}>
            {this.props.value}
          </code>
        </pre>
        }
      </div>
    )
  }
}

CodeBlock.defaultProps = {
  language: ''
}

CodeBlock.propTypes = {
  value: PropTypes.string.isRequired,
  language: PropTypes.string
}

module.exports = CodeBlock
