import React, {PropTypes, Component} from 'react';
import Editor from '../components/editor'
import RichEditor from '../components/richEditor'
import Preview from '../containers/preview'
import Help from '../components/help'

let titleInput,
  textInput;
export default class Details extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {todo} = this.props;
    if (!todo)
      return null;
    return (
      <div className="full-height pb-2">
        {
          this.props.parents.length > 0 && <nav aria-label="breadcrumb">
              <ol className="breadcrumb rounded-0">
                {
                  this.props.parents.reverse().map(
                    (v, k) => <li key={k} className="breadcrumb-item">
                      <button
                        className="btn btn-link text-truncate"
                        onClick={e => this.props.onSelectTodo(v.id)}
                        style={{
                          maxWidth: "6em",
                          padding: 0
                        }}>{v.title}</button>
                    </li>
                  )
                }
              </ol>
            </nav>
        }

        {/* <RichEditor todo={todo}/> */}
        <Preview className={todo.edit
            ? "d-none"
            : ""} todo={todo}/>
        <Editor
          className={todo.edit
            ? ""
            : "d-none"}
          todo={todo}
          updateTitle={this.props.updateTitle}
          updateText={this.props.updateText}/>
      </div>
    );
  }

}
