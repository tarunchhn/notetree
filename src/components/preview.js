import React, {PropTypes, Component} from 'react';
//import 'highlight.js/styles/idea.css';
//import './preview.css';
import {MessageModal, copyData} from './util'
import CodeBlock from './markdown/codeBlock'
var ReactDOM = require('react-dom');
var ReactMarkdown = require('react-markdown');

export default class Preview extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.startEditor = this.startEditor.bind(this);
  }

  startEditor() {
    this.setState({title: this.props.todo.title, text: this.props.todo.text});
  }

  render() {
    const {todo} = this.props;
    if (!todo)
      return null;
    return (
      <div className={"full-height ml-3 preview ".concat(this.props.className)}>

        <div className="">
          <label
            id="title"
            type="text"
            className="no-border text-center pr-2"
            style={{
              fontSize: "xx-large",
              width: "100%"
            }}>{todo.title}

            <div className="dropdown d-inline ml-2">
              <a
                className="btn dropdown-toggle"
                href="#"
                role="button"
                id="dropdownMenuLink"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false">
              </a>

              <div className="dropdown-menu border border-primary-50 shadow-sm" aria-labelledby="dropdownMenuLink">
                <a className="dropdown-item text-primary">Share</a>
                <a className="dropdown-item text-primary" href="slack://open" onClick={e => {
                  copyData(e, "".concat(todo.title).concat("\r\n").concat(todo.text))
                }}>Share via slack</a>
                <a className="dropdown-item text-primary" onClick={e => {
                  copyData(e, "".concat(todo.title).concat("\r\n").concat(todo.text))
                }}>Copy raw content</a>
              </div>
            </div>
          </label>
        </div>
        <div className="pt-2">
          <div className="full-height mr-3">
            <ReactMarkdown
              escapeHtml={!todo.renderHtml}
              renderers={{
                code: CodeBlock,
                table: props => React.createElement('table', {
                  'data-sourcepos': props['data-sourcepos'],
                  className: 'table table-hover table-bordered'
                }, props.children),
                list: props => {
                  const attrs = {
                    'data-sourcepos': props['data-sourcepos'],
                    className: 'disc pl-3 ml-1'
                  }
                  if (props.start !== null && props.start !== 1) {
                    attrs.start = props.start.toString()
                  }

                  return React.createElement(
                    props.ordered
                      ? 'ol'
                      : 'ul',
                    attrs,
                    props.children
                  )
                }
              }}
              source={todo.text || ""}/>
          </div>
        </div>
      </div>
    );
  }

}
