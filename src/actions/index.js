export const addTodo = (id, title, parentId) => {
  return {
    type: 'ADD_TODO',
    id: id,
    parentId: parentId,
    title: title
  }
}

export const selectTodo = (id) => {
  return {
    type: 'SELECT_TODO',
    id: id
  }
}

export const setVisibilityFilter = (filter) => {
  return { type: 'SET_VISIBILITY_FILTER', filter }
}

export const toggleTodo = (id) => {
  return { type: 'TOGGLE_TODO', id }
}

export const updateTodo = (id) => {
  return { type: 'UPDATE_TODO', id: id };
}

export const updateTitle = (id, title) => {
  return { type: 'UPDATE_TITLE', id: id, title: title };
}

export const hideTodo = (ids, hide) => {
  return { type: 'HIDE_TODO', ids: ids, hide: hide };
}

export const updateText = (id, text) => {
  return { type: 'UPDATE_TEXT', id: id, text: text };
}

export const updateRenderHtml = (id, renderHtml) => {
  return { type: 'UPDATE_RENDER_HTML', id: id, renderHtml: renderHtml };
}

export const deleteTodo = (id) => {
  return { type: 'DELETE_TODO', id: id };
}

export const editTodo = (id) => {
  return {type: 'EDIT_TODO', id: id}
}

export const cancelEdit = (id) => {
  return {type: 'CANCEL_EDIT', id: id}
}

export const moveTodo = (moveInfo) => {
  return {type: 'MOVE_TODO', moveInfo: moveInfo}
}

export const search = (searchTerm) => {
  return {type: 'SEARCH', searchTerm: searchTerm}
}

export const onSplitPaneSizeChange = (size) => {
  return {type: 'SPLIT_PANE_SIZE_CHANGE', size: size}
}
