import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React from 'react';
import ReactDOM from 'react-dom';
//import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import WebFont from 'webfontloader';
import '@fortawesome/fontawesome-free/css/all.css';
// this shell will only be available if the app is run in electron
var shell;
if (window.require) {
  shell = window.require('electron').shell;
}

WebFont.load({
  google: {
    families: ['Montserrat:300,400,700', 'sans-serif']
  }
});

ReactDOM.render( < App / > , document.getElementById('root'));

$(function() {
  var theme = localStorage.getItem('theme');
  if (!theme) {
    theme = "theme-green";
  }
  $('head').append('<link id="themeFile" rel="stylesheet" type="text/css" href="static/css/' + theme + '.css">');
  $('[data-toggle="tooltip"]').tooltip()
  $('.popover-dismiss').popover({
    trigger: 'focus'
  })
  //if the app is being run in electron, then open links in external application (browser)
  if (shell) {
    $(document).on('click', 'a[href^="http|mailto|slack"]', function(event) {
      event.preventDefault();
      shell.openExternal(this.href);
    })
  }
})

registerServiceWorker();
