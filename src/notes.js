import React, {Component} from 'react';
import {connect} from 'react-redux'
import Header from './containers/header.js';
import Tree from './containers/tree.js';
import Details from './containers/details.js';
import Search from './containers/search.js';
import {onSplitPaneSizeChange} from './actions'
import SplitPane from 'react-split-pane'
import {DragDropContext} from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

const mapStateToProps = (state, ownProps) => {
  return {size: state.support.splitPaneSize}
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSplitPaneSizeChange: (size) => {
      dispatch(onSplitPaneSizeChange(size))
    }
  }
}

class Notes extends Component {

  constructor() {
    super()
  }

  render() {
    return (
      <div className="container-fluid full-height">
        <Header/>
        <SplitPane
          style={{
            paddingTop: "3em"
          }}
          split="vertical"
          minSize={50}
          defaultSize={this.props.size}
          onChange={size => this.props.onSplitPaneSizeChange(size)}
          pane2Style={{overflow: "scroll"}}>
          <div className="full-height scrollable mr-2 mt-2" style={{
              width: this.props.size + "px"
            }}>
            <Search/>
            <Tree root="0"/>
          </div>
          <div className="full-height scrollable">
            <Details/>
          </div>
        </SplitPane>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DragDropContext(HTML5Backend)(Notes));
